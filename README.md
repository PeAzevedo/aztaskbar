![](https://gitlab.com/arcmenu/arcmenu-assets/raw/master/images/azTaskbar.png)

-----
### Introduction

App Icons Taskbar (azTaskbar) is a simple taskbar extension for GNOME Shell, designed to provide a more familiar user experience and workflow. This extension places app icons in the panel showing current running apps, and GNOME favorites.

Picture shown above uses [ArcMenu](https://extensions.gnome.org/extension/3628/arcmenu/) to add an application menu to the panel.

-----

### Installation

#### To install the most recent official release: Visit App Icons Taskbar on the [Official GNOME Extensions](https://extensions.gnome.org/extension/4944/app-icons-taskbar/) website.

- **To install App Icons Taskbar from source**: Please see the: [Install From Source wiki Guide](https://gitlab.com/AndrewZaech/aztaskbar/-/wikis/Install-From-Source-Guide).

<p align="left">
       <a href="https://extensions.gnome.org/extension/4944/app-icons-taskbar" >
    <img src="https://gitlab.com/arcmenu/arcmenu-assets/raw/master/images/get-it-ego.png" width="240" style="margin-left: 4px"/>
    </a>

-----

### Support App Icons Taskbar

App Icons Taskbar is provided free of charge. If you enjoy using this extension and wish to help support the project, feel free to use the link below!

[![Donate via Paypal](https://gitlab.com/arcmenu/arcmenu-assets/raw/master/images/paypal_donate.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=53CWA7NR743WC&item_name=Donate+to+support+my+work&currency_code=USD&source=url)

-----

### Credits:

**@[AndrewZaech](https://gitlab.com/AndrewZaech) - Project Maintainer and Developer**

Additional credits:
* Large parts of the window preview feature copied from Dash to Dock extension and modified for this extensions needs.
* AppIcon mouse scroll cycle window logic borrowed from Dash to Panel (https://github.com/home-sweet-gnome/dash-to-panel/blob/master/utils.js#L415-L430)
* Window peeking feature based on code from Dash to Panel

-----

### Contributors:

#### App Icons Taskbar Logo

Created by **@[AndyC](https://gitlab.com/LinxGem33)** and licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

#### Thanks to the following people for contributing via merge requests:

**@[wednesbunny](https://gitlab.com/wednesbunny)**


#### Translators:

**@[daPhipz](https://gitlab.com/daPhipz), @[Etamuk](https://gitlab.com/Etamuk) - German** | **@[Vistaus](https://gitlab.com/Vistaus) - Dutch** | **@[celeri](https://gitlab.com/celestomm) - French** | **@[Martin Torres](https://gitlab.com/martttin) - Spanish**

-----
